package org.acme;

import org.acme.model.Tag;
import org.acme.network.BaseResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("/tag")
public class TagResource {
    public static List<Tag> tags = new ArrayList<>();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTags() {
        try {
            BaseResponse resp = new BaseResponse();
            resp.setData(tags);
            resp.setCode(Response.Status.OK.getStatusCode());
            resp.setMessage("success");
            return Response.ok(resp).build();
        } catch (Exception e) {
            System.out.println(e);
            return Response.ok(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTags(Tag t) {
        try {
            tags.add(t);
            BaseResponse resp = new BaseResponse();
            resp.setData(tags);
            resp.setCode(Response.Status.CREATED.getStatusCode());
            resp.setMessage("success");
            return Response.ok(resp).build();
        } catch (Exception e) {
            System.out.println(e);
            return Response.ok(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response udapteTags(Tag t) {
        try{
            tags = tags.stream().map(ps -> {
                ps.setID(t.getID());
                ps.setLabel(t.getLabel());
                ps.setPost(t.getPost());

                return ps;
            }).collect(Collectors.toList());

            BaseResponse resp = new BaseResponse();
            resp.setData(tags);
            resp.setCode(Response.Status.OK.getStatusCode());
            resp.setMessage("success");
            return Response.ok(resp).build();
        } catch (Exception e) {
            System.out.println(e);
            return Response.ok(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteTags(
            @PathParam("id") int id
    ) {
        try {
            Optional<Tag> pd = tags.stream().filter(ps -> ps.getID() == id).findFirst();

            boolean removed = tags.remove(pd);
            return removed ? Response.noContent().build() : Response.status(Response.Status.BAD_REQUEST).build();
        } catch (Exception e) {
            System.out.println(e);
            return Response.ok(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
