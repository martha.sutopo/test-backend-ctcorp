package org.acme;

import org.acme.model.Post;
import org.acme.network.BaseResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("/post")
public class PostResource {
    public static List<Post> posts = new ArrayList<>();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPosts() {
        try {
            BaseResponse resp = new BaseResponse();
            resp.setData(posts);
            resp.setCode(Response.Status.OK.getStatusCode());
            resp.setMessage("success");
            return Response.ok(resp).build();
        } catch (Exception e) {
            System.out.println(e);
            return Response.ok(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createPosts(Post p) {
        try {
            posts.add(p);
            BaseResponse resp = new BaseResponse();
            resp.setData(posts);
            resp.setCode(Response.Status.CREATED.getStatusCode());
            resp.setMessage("success");
            return Response.ok(resp).build();
        } catch (Exception e) {
            System.out.println(e);
            return Response.ok(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response udaptePosts(Post p) {
        try {
            posts = posts.stream().map(ps -> {
                ps.setID(p.getID());
                ps.setTitle(p.getTitle());
                ps.setContent(p.getContent());
                ps.setTags(p.getTags());

                return ps;
            }).collect(Collectors.toList());

            BaseResponse resp = new BaseResponse();
            resp.setData(posts);
            resp.setCode(Response.Status.OK.getStatusCode());
            resp.setMessage("success");
            return Response.ok(resp).build();
        } catch (Exception e) {
            System.out.println(e);
            return Response.ok(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deletePosts(
            @PathParam("id") int id
    ) {
        try {
            Optional<Post> pd = posts.stream().filter(ps -> ps.getID() == id).findFirst();
            boolean removed = posts.remove(pd);
            return removed ? Response.noContent().build() : Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            System.out.println(e);
            return Response.ok(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

}
