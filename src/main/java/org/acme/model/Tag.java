package org.acme.model;

import org.acme.model.Post;

import java.util.List;

public class Tag {
    private int ID;
    private String Label;
    private List<Post> post;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public List<Post> getPost() {
        return post;
    }

    public void setPost(List<Post> post) {
        this.post = post;
    }
}
